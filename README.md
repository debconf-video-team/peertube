Tools and metadata for managing the PeerTube mirror of the Debian
Meetings archive.

The "metadata" directory contains YAML files describing playlists of videos.
These are built from metadata from the "archive-meta" repo.

"auth.py" will just authenticate to PeerTube.

"upload_peertube.py" will upload to PeerTube, as the name describes. It
can write a log of the peertube_ids, for merging back into metadata.

"merge-meta.py" will merge metadata changes from the "archive-meta" repo.

"merge-uploads.py" merges a log from "upload_peertube.py" back into the metadata.

"publish-slowly.py" will publish a video a day.

[Documentation](https://debconf-video-team.pages.debian.net/peertube/)
