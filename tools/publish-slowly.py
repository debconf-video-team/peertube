#!/usr/bin/env python3

import argparse
import logging
import sys
import time
import yaml

from client.constants import VideoPrivacy, VideoState
from client.video import Video


def load_manifest(manifest):
    return yaml.safe_load(manifest)


def publish(peertube_id):
    """Publish a video

    Return True if the video was previously not public
    """
    video = Video.get(peertube_id)
    if video.privacy == VideoPrivacy.PUBLIC:
        return False
    if video.state != VideoState.PUBLISHED:
        return False
    video.publish()
    return True


def main():
    p = argparse.ArgumentParser()
    p.add_argument('manifest', metavar='MANIFEST', type=open,
                   help='YAML upload manifest')
    p.add_argument('--batch', metavar='NUMBER', type=int, default=1,
                   help='Publish NUMBER of videos in every patch.')
    p.add_argument('--wait', metavar='DAYS', type=int, default=1,
                   help='Wait DAYS days between each batch of publishes.')
    p.add_argument('--verbose', '-v', action='store_true',
                   help='Increase verbosity')
    args = p.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)

    manifest = load_manifest(args.manifest)
    peertube_ids = (video['peertube_id']
                 for video in manifest['videos']
                 if 'peertube_id' in video)

    while True:
        for i in range(args.batch):
            for peertube_id in peertube_ids:
                if publish(peertube_id):
                    break
            else:
                # Everything is published
                sys.exit(0)

        time.sleep(args.wait * 24 * 60 * 60)


if __name__ == '__main__':
    main()
