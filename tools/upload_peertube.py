#!/usr/bin/env python3

import argparse
import logging
import signal
import sys

import yaml

from client.playlist import Playlist
from client.video import Video


def load_manifest(manifest):
    return yaml.safe_load(manifest)


def post_peertube(meta, logfile):
    """Post video to PeerTube and log it"""
    playlist = meta.pop('playlist', None)
    peertube_id = meta.pop('peertube_id', None)
    if not peertube_id:
        channel_id = meta.pop('channel_id')
        url = meta.pop('url')
        log = {
            'url': url,
        }
        video = Video.import_video(channel_id, url, **meta)

        log['peertube_id'] = video.uuid
        yaml.safe_dump(log, logfile, explicit_start=True)
        logfile.flush()
        if playlist:
            Playlist.get(playlist).add(video)
    else:
        video = Video.get(peertube_id)
        video.set_description(meta['description'])


def main():
    p = argparse.ArgumentParser()
    p.add_argument('manifest', metavar='MANIFEST', type=open,
                   help='YAML upload manifest.')
    p.add_argument('log', metavar='LOG', type=argparse.FileType('a'),
                   help='File to log uploads to.')
    p.add_argument('--verbose', '-v', action='store_true',
                   help='Increase verbosity.')
    args = p.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)

    manifest = load_manifest(args.manifest)

    video_defaults = manifest.get('defaults', {})

    for video in manifest['videos']:
        # Apply defaults
        for k, v in video_defaults.items():
            if k in video and isinstance(video[k], list):
                video[k].extend(v)
            else:
                video.setdefault(k, v)

        try:
            post_peertube(video, args.log)
        except KeyboardInterrupt:
            sys.stderr.write('Aborted (^C)\n')
            sys.exit(128 + signal.SIGINT)


if __name__ == '__main__':
    main()
