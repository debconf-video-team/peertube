import logging

from client.peertube import get_session
from client.video import Video

log = logging.getLogger(__name__)


class Playlist:
    def __init__(self, data):
        self._pt = get_session()
        self.data = data

    @property
    def uuid(self):
        return self.data['uuid']

    @property
    def name(self):
        return self.data['name']

    @property
    def videos(self):
        r = self._pt.get('video-playlists/{}/videos'.format(self.uuid))
        # TODO Pagination
        print(r.json())

    def add(self, video):
        video_id = video
        if isinstance(video, Video):
            video_id = video.uuid
        data = {
            'videoId': video_id,
        }
        log.info('Adding %s to playlist %s', video_id, self.uuid)
        self._pt.post('video-playlists/{}/videos'.format(self.uuid),
                      data=data)

    def update(self, **data):
        self._pt.put('video-playlists/{}'.format(self.uuid), data=data)
        self.data.update(data)

    def delete(self):
        self._pt.delete('video-playlists/{}'.format(self.uuid))

    @classmethod
    def get(cls, id_):
        peertube = get_session()
        response = peertube.get('video-playlists/{}'.format(id_)).json()
        return Playlist(response)
