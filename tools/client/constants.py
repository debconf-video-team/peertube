from enum import IntEnum


class VideoPrivacy(IntEnum):
    PUBLIC = 1
    UNLISTED = 2
    PRIVATE = 3
    INTERNAL = 4


class VideoState(IntEnum):
    PUBLISHED = 1
    TO_TRANSCODE = 2
    TO_IMPORT = 3
