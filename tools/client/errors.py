class PeerTubeError(Exception):
    def __init__(self, msg, code):
        super().__init__(msg)
        self.code = code


class RateLimitedException(Exception):
    pass
