from client.peertube import get_session


class User:
    @property
    def _pt(self):
        return get_session()

    def me(self):
        return self._pt.get('users/me').json()
