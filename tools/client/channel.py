from client.peertube import get_session


class Channel:
    def __init__(self, data):
        self._pt = get_session()
        self.data = data

    @classmethod
    def get_channels(cls):
        peertube = get_session()
        return peertube.get('video-channels').json()
