#!/usr/bin/env python3

from getpass import getpass
from pprint import pprint
import argparse
import logging
import sys

from client.errors import PeerTubeError
from client.peertube import (
    CLIENT_CONFIG_FILE, TOKEN_FILE, PeerTubeSession, fetch_client_keypair)
from client.user import User


def main():
    p = argparse.ArgumentParser(
        'Log into PeerTube and generate client_id.json and token.json')
    p.add_argument('--verbose', '-v', action='store_true',
                   help='Increase verbosity.')
    p.add_argument('--url',
                   help='PeerTube URL base. '
                   'Read from client_id.json if not specified.')
    p.add_argument('--account', metavar='ACCOUNT',
                   help='Log into PeerTube with ACCOUNT')
    p.add_argument('--password',
                   help='PeerTube password (will prompt if not specified)')
    args = p.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)

    if args.account and not args.password:
        args.password = getpass()

    if args.url:
        fetch_client_keypair(args.url)
    elif not CLIENT_CONFIG_FILE.exists():
        print('{} is missing.\nRe-run with --url to obtain a client_id.'
              .format(CLIENT_CONFIG_FILE.name), file=sys.stderr)
        sys.exit(1)

    if args.account:
        pt = PeerTubeSession(load_token=False)
        pt.fetch_token(args.account, args.password)
    elif not TOKEN_FILE.exists():
        print('{} is missing.\nRe-run with --account to obtain a new token.'
              .format(TOKEN_FILE.name), file=sys.stderr)
        sys.exit(1)
    else:
        pt = PeerTubeSession()

    try:
        user = User().me()
    except PeerTubeError as e:
        if e.code == 'invalid_token':
            print('{}\nRe-run with --acount to obtain a new token.'.format(e),
                  file=sys.stderr)
            sys.exit(1)
        else:
            raise

    print("Logged in. Account:")
    pprint(user)


if __name__ == '__main__':
    main()
