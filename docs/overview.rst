Overview
========

DebConf maintains an archive of current and historical conference videos, `meetings-archive`_.
And a git repository describing every video, with metadata, `archive-meta`_.

.. _meetings-archive: https://meetings-archive.debian.net/pub/debian-meetings
.. _archive-meta: https://salsa.debian.org/debconf-video-team/archive-meta

This tool exists to maintain a mirror of this video archive in PeerTube,
to reach a wider audience, and provide a user-friendly, albeit non-free,
player interface.

The ``metadata`` directory contains YAML files describing playlists of videos.
These are built from metadata from the "archive-meta" repo, with the
``merge-meta.py`` tool.
